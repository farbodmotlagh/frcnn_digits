try:
    import cPickle as pickle
except ImportError:
    pass

try:
    import _pickle as pickle
except ImportError:
    pass

from datasets.imdb import imdb
import os
import numpy as np
import scipy
from model.config import cfg

from sklearn.metrics.pairwise import pairwise_distances
from scipy.optimize import linear_sum_assignment

class balls(imdb):
    def __init__(self, image_set):
        imdb.__init__(self, 'balls')
        self._classes = ('__background__', 'ball')
        # self._classes = ('__background__', 'red', 'green', 'blue', 'yellow')
        self._image_set = image_set
        self._image_ext = '.jpg'
        self._data_path = os.path.join(cfg.DATA_DIR, 'ds-card-high-overlap2')
        self._class_to_ind = dict(list(zip(self.classes, list(range(self.num_classes)))))
        self._image_index = self._load_image_set_index()
        self._roidb_handler = self.gt_roidb
        self._image_index_formatter = '{0:0>' + str(len(self._image_index) - 1) + '}'

    def _load_image_set_index(self):
        images_path = os.path.join(self._data_path, self._image_set)
        image_index = np.array(sorted([os.path.join(images_path, fn) for fn in os.listdir(images_path) if fn.endswith(self._image_ext)]))
        return image_index

    def gt_roidb(self):
        cache_file = os.path.join(self.cache_path, self.name + '_' + self._image_set + '_gt_roidb.pkl')

        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as fid:
                try:
                    roidb = pickle.load(fid)
                except:
                    roidb = pickle.load(fid, encoding='bytes')
            print('{} gt roidb loaded from {}'.format(self.name, cache_file))
            return roidb

        gt_labels = np.load(os.path.join(self._data_path, self._image_set + '_labels.npz'))['a']
        gt_roidb = [self._get_annotation(gt_label) for gt_label in gt_labels]

        with open(cache_file, 'wb') as fid:
            pickle.dump(gt_roidb, fid, pickle.HIGHEST_PROTOCOL)
        print('wrote gt roidb to {}'.format(cache_file))

        return gt_roidb

    def image_path_from_index(self, index):
        image_path = os.path.join(self._data_path, self._image_set,
                                  index)
        assert os.path.exists(image_path), \
            'Path does not exist: {}'.format(image_path)
        return image_path

    def image_path_at(self, i):
        return self.image_path_from_index(self._image_index[i])

    def _get_annotation(self, gt_bboxes):

        valid_gt_bboxes = gt_bboxes[~np.all(gt_bboxes == 0, axis=1)]
        num_objs = len(valid_gt_bboxes)

        boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)
        # "Seg" area for pascal is just the box area
        seg_areas = np.zeros((num_objs), dtype=np.float32)

        # Load object bounding boxes into a data frame.
        for ix, obj in enumerate(valid_gt_bboxes):
            x1 = obj[0]
            y1 = obj[1]
            x2 = obj[0] + obj[2]
            y2 = obj[1] + obj[3]

            cls = 1
            boxes[ix, :] = [x1, y1, x2, y2]
            gt_classes[ix] = cls
            overlaps[ix, cls] = 1.0
            seg_areas[ix] = obj[2] * obj[3]

        overlaps = scipy.sparse.csr_matrix(overlaps)

        return {'boxes': boxes,
                'gt_classes': gt_classes,
                'gt_overlaps': overlaps,
                'flipped': False,
                'seg_areas': seg_areas}

    def evaluate_detections(self, all_boxes, output_dir):
        labels = np.load(os.path.join(self._data_path, self._image_set + '_labels.npz'))

        all_gt_bboxes = labels['a']
        all_gt_card = np.argmax(labels['b'], axis=1)
        # all_gt_scores = labels['c']

        valid_card_count = 0.

        confidence_idx = 4

        valid_gt_boxes = []
        valid_p_boxes = []

        wrong_card_indices = []

        for i, p_box_container in enumerate(all_boxes):
            p_boxes = [p_box for p_box in p_box_container[1] if p_box[confidence_idx] > 0.5]
            predicted_boxes_count = len(p_boxes)

            # print "Predicted count: {}, gt card: {}".format(predicted_boxes_count, all_gt_card[i])

            # print "Gt: {}, Pred: {}".format(all_gt_card[i], predicted_boxes_count)
            if predicted_boxes_count != all_gt_card[i]:
                wrong_card_indices.append(i)
            else:
                valid_card_count += 1
                if predicted_boxes_count > 0:
                  p_boxes_np = np.array(p_boxes)[:, :4]
                  p_boxes_np[:, 2:4] = p_boxes_np[:, 2:4] - p_boxes_np[:, 0:2]

                  if predicted_boxes_count < 4:
                      padding = np.zeros((4 - predicted_boxes_count, 4))
                      p_boxes_np = np.vstack((p_boxes_np, padding))

                  gt_bboxes = all_gt_bboxes[i]

                  dis = pairwise_distances(p_boxes_np, gt_bboxes)
                  _, nperm = linear_sum_assignment(dis)
                  new_gt_boxes = gt_bboxes[nperm]

                  valid_gt_boxes.append(new_gt_boxes)
                  valid_p_boxes.append(p_boxes_np)
                else:
                  valid_p_boxes.append(np.zeros((4, 4)))
                  valid_gt_boxes.append(all_gt_bboxes[i])

        valid_p_boxes = np.array(valid_p_boxes)
        valid_gt_boxes = np.array(valid_gt_boxes)

        mse = ((valid_p_boxes - valid_gt_boxes) ** 2).mean()
        card_accuracy = valid_card_count / len(all_gt_bboxes)

        print 'Mse: {}, Cardinality accuracy: {}'.format(mse, card_accuracy)
        return mse, card_accuracy, np.array(wrong_card_indices)