# --------------------------------------------------------
# Fast/er R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Bharath Hariharan
# --------------------------------------------------------
from __future__ import absolute_import
# from __future__ import division
# from __future__ import print_function

import xml.etree.ElementTree as ET
import os
import pickle
import numpy as np
import json
import os.path as osp
import scipy.io as sio

def parse_rec(filename):
  """ Parse a PASCAL VOC xml file """
  tree = ET.parse(filename)
  objects = []
  for obj in tree.findall('object'):
    obj_struct = {}
    obj_struct['name'] = obj.find('name').text
    obj_struct['pose'] = obj.find('pose').text
    obj_struct['truncated'] = int(obj.find('truncated').text)
    obj_struct['difficult'] = int(obj.find('difficult').text)
    bbox = obj.find('bndbox')
    obj_struct['bbox'] = [int(bbox.find('xmin').text),
                          int(bbox.find('ymin').text),
                          int(bbox.find('xmax').text),
                          int(bbox.find('ymax').text)]
    objects.append(obj_struct)

  return objects

def parse_annotations(bboxes_per_image):
  present_boxes = bboxes_per_image[~np.all(bboxes_per_image == 0, axis=1)]

  objects = []
  for box in present_boxes:
    obj_struct = {}
    obj_struct['name'] = 'object'
    obj_struct['difficult'] = False
    obj_struct['bbox'] = [int(box[0]), int(box[1]), int(box[0] + box[2]), int(box[1] + box[3])]
    objects.append(obj_struct)

  return objects

def parse_annotations_matlab(all_bboxes):
  bboxes = []

  for bboxes_per_img in all_bboxes:
    present_boxes = bboxes_per_img[~np.all(bboxes_per_img == 0, axis=1)]

    bboxes_img = []
    for box in present_boxes:
      bboxes_img.append([int(box[0]), int(box[1]), int(box[2]), int(box[3]), 0])
    bboxes.append(bboxes_img)

  return np.array(bboxes)


def read_image_names(path):
  images_path = osp.join(path, 'test')
  image_ids = sorted([fn for fn in os.listdir(images_path) if fn.endswith('.jpg')])
  return image_ids

def read_annotations(path):
  labels_filename = osp.join(path, 'test_labels.npz')
  return np.load(labels_filename)['a']

def voc_ap(rec, prec, use_07_metric=False):
  """ ap = voc_ap(rec, prec, [use_07_metric])
  Compute VOC AP given precision and recall.
  If use_07_metric is true, uses the
  VOC 07 11 point method (default:False).
  """
  if use_07_metric:
    # 11 point metric
    ap = 0.
    for t in np.arange(0., 1.1, 0.1):
      if np.sum(rec >= t) == 0:
        p = 0
      else:
        p = np.max(prec[rec >= t])
      ap = ap + p / 11.
  else:
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.], rec, [1.]))
    mpre = np.concatenate(([0.], prec, [0.]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
      mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
  return ap


def evaluate_prec_rec_with_card(fp, npos, tp, use_07_metric):
  fp = np.sum(fp)
  tp = np.sum(tp)

  rec = tp / float(npos)
  prec = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
  return None, prec, rec

def evaluate_prec_recall(fp, npos, tp, use_07_metric):
  fp = np.cumsum(fp)
  tp = np.cumsum(tp)
  rec = tp / float(npos)
  # avoid divide by zero in case the first detection matches a difficult
  # ground truth
  prec = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
  ap = voc_ap(rec, prec, use_07_metric)
  return ap, prec, rec

def voc_eval(detpath,
             annopath,
             imagesetfile,
             classname,
             cachedir,
             get_detects,
             ovthresh=0.5,
             eval_results=evaluate_prec_recall,
             use_07_metric=False,
             use_diff=False):
  """rec, prec, ap = voc_eval(detpath,
                              annopath,
                              imagesetfile,
                              classname,
                              [ovthresh],
                              [use_07_metric])

  Top level function that does the PASCAL VOC evaluation.

  detpath: Path to detections
      detpath.format(classname) should produce the detection results file.
  annopath: Path to annotations
      annopath.format(imagename) should be the xml annotations file.
  imagesetfile: Text file containing the list of images, one image per line.
  classname: Category name (duh)
  cachedir: Directory for caching the annotations
  [ovthresh]: Overlap threshold (default = 0.5)
  [use_07_metric]: Whether to use VOC07's 11 point AP computation
      (default False)
  """
  # assumes detections are in detpath.format(classname)
  # assumes annotations are in annopath.format(imagename)
  # assumes imagesetfile is a text file with each line an image name
  # cachedir caches the annotations in a pickle file

  # first load gt
  if not os.path.isdir(cachedir):
    os.mkdir(cachedir)
  cachefile = os.path.join(cachedir, '%s_annots.pkl' % imagesetfile)

  # read list of images
  imagenames = read_image_names(annopath)
  annotations = read_annotations(annopath)

  recs = load_recs(annotations, cachefile, imagenames)
  # extract gt objects for this class
  class_recs, npos = get_class_recs(classname, imagenames, recs, use_diff)

  # read dets
  BB, confidence, image_ids = get_detects(detpath, imagenames)

  nd = len(image_ids)
  tp = np.zeros(nd)
  fp = np.zeros(nd)

  if BB.shape[0] > 0:
    # sort by confidence
    sorted_ind = np.argsort(-confidence)
    sorted_scores = np.sort(-confidence)
    BB = BB[sorted_ind, :]
    image_ids = [image_ids[x] for x in sorted_ind]

    # go down dets and mark TPs and FPs
    for d in range(nd):
      R = class_recs[image_ids[d]]
      bb = BB[d, :].astype(float)
      ovmax = -np.inf
      BBGT = R['bbox'].astype(float)

      if BBGT.size > 0:
        # compute overlaps
        # intersection
        ixmin = np.maximum(BBGT[:, 0], bb[0])
        iymin = np.maximum(BBGT[:, 1], bb[1])
        ixmax = np.minimum(BBGT[:, 2], bb[2])
        iymax = np.minimum(BBGT[:, 3], bb[3])
        iw = np.maximum(ixmax - ixmin + 1., 0.)
        ih = np.maximum(iymax - iymin + 1., 0.)
        inters = iw * ih

        # union
        uni = ((bb[2] - bb[0] + 1.) * (bb[3] - bb[1] + 1.) +
               (BBGT[:, 2] - BBGT[:, 0] + 1.) *
               (BBGT[:, 3] - BBGT[:, 1] + 1.) - inters)

        overlaps = inters / uni
        ovmax = np.max(overlaps)
        jmax = np.argmax(overlaps)

      if ovmax > ovthresh:
        if not R['difficult'][jmax]:
          if not R['det'][jmax]:
            tp[d] = 1.
            R['det'][jmax] = 1
          else:
            fp[d] = 1.
      else:
        fp[d] = 1.

  # compute precision recall
  ap, prec, rec = eval_results(fp, npos, tp, use_07_metric)
  return rec, prec, ap


def get_detections(classname, detpath):
  detfile = detpath.format(classname)
  with open(detfile, 'r') as f:
    lines = f.readlines()
  splitlines = [x.strip().split(' ') for x in lines]
  image_ids = [x[0] for x in splitlines]
  confidence = np.array([float(x[1]) for x in splitlines])
  BB = np.array([[float(z) for z in x[2:]] for x in splitlines])
  return BB, confidence, image_ids


def get_detections_yolo(detpath, image_ids):
  filenames = sorted([osp.join(detpath, fn) for fn in os.listdir(detpath) if fn.endswith('.json')])

  detection_image_ids = []
  confidences = []
  bboxes = []

  for image_id, filename in zip(image_ids, filenames):
    with open(filename) as json_data:

      data = json.load(json_data)
      for obj in data:
        detection_image_ids.append(image_id)
        confidences.append(obj['confidence'])

        topleft = obj['topleft']
        bottomright = obj['bottomright']
        bboxes.append([int(topleft['x']), int(topleft['y']), int(bottomright['x']), int(bottomright['y'])])

  return np.array(bboxes), np.array(confidences), detection_image_ids

def get_detections_yolo_matlab(detpath):
  filenames = sorted([osp.join(detpath, fn) for fn in os.listdir(detpath) if fn.endswith('.json')])

  bboxes = []

  for filename in filenames:
    with open(filename) as json_data:

      data = json.load(json_data)
      img_bboxes = []
      for obj in data:

        topleft = obj['topleft']
        bottomright = obj['bottomright']

        x = topleft['x']
        y = topleft['y']
        width = bottomright['x'] - x
        height = bottomright['y'] - y
        confidence = obj['confidence']

        bbox = [int(x), int(y), int(width), int(height), confidence]
        img_bboxes.append(bbox)

      bboxes.append(img_bboxes)

  return np.array(bboxes)

def get_detections_facter_rcnn(detpath, image_ids):
  detections = np.array(pickle.load(open(detpath, 'rb'))).T

  detection_image_ids = []
  confidences = []
  bboxes = []

  score_idx = 4

  for image_id, p_box_container in zip(image_ids, detections):
    # p_boxes = [p_box for p_box in p_box_container[1] if p_box[score_idx] > 0.6]
    p_boxes = [p_box for p_box in p_box_container[1]]

    for p_box in p_boxes:
      detection_image_ids.append(image_id)
      confidences.append(p_box[score_idx])
      bboxes.append([int(p_box[0]), int(p_box[1]), int(p_box[2]), int(p_box[3])])

  return np.array(bboxes), np.array(confidences), detection_image_ids

def get_detections_facter_rcnn_matlab(detpath):
  detections = np.array(pickle.load(open(detpath, 'rb'))).T
  bboxes = []

  for p_box_container in detections:
    img_pboxes = []

    p_boxes = [p_box for p_box in p_box_container[1]]

    for p_box in p_boxes:
      bbox = [int(p_box[0]), int(p_box[1]), int(p_box[2] - p_box[0]), int(p_box[3] - p_box[1]), p_box[4]]
      img_pboxes.append(bbox)

    bboxes.append(img_pboxes)

  return np.array(bboxes)

def get_detections_balls_net_no_card(detpath, image_ids):
  detections = pickle.load(open(detpath, 'rb'))

  bboxes = detections['bboxes']
  p_scores = detections['p_scores']

  detection_image_ids = []
  confidences = []
  valid_bboxes = []

  for image_id, p_boxes, p_scores_img in zip(image_ids, bboxes, p_scores):
    for p_box, p_score in zip(p_boxes, p_scores_img):

      # if p_score >= 0.6:
      detection_image_ids.append(image_id)
      confidences.append(p_score)
      valid_bboxes.append([int(p_box[0]), int(p_box[1]), int(p_box[0] + p_box[2]), int(p_box[1] + p_box[3])])

  return np.array(valid_bboxes), np.array(confidences), detection_image_ids


def get_detections_balls_net_no_card_matlab(detpath):
  detections = pickle.load(open(detpath, 'rb'))

  bboxes = detections['bboxes']
  p_scores = detections['p_scores']

  valid_bboxes = np.empty(len(bboxes), dtype=list)

  for i, (p_boxes, p_scores_img) in enumerate(zip(bboxes, p_scores)):
    img_pboxes = []

    sigm = lambda x: 1. / (1. + np.exp(-x))
    for p_box, p_score in zip(p_boxes, p_scores_img):
      entry = [int(p_box[0]), int(p_box[1]), int(p_box[2]), int(p_box[3]), sigm(p_score)]
      img_pboxes.append(entry)

    valid_bboxes[i] = img_pboxes

  return valid_bboxes

def get_detections_balls_net_card(detpath, image_ids):
  detections = pickle.load(open(detpath, 'rb'))

  bboxes = detections['bboxes']
  presence = detections['presence']
  p_scores = detections['p_scores']

  detection_image_ids = []
  confidences = []
  valid_bboxes = []

  for image_id, p_boxes, p_scores_img, presence_img in zip(image_ids, bboxes, p_scores, presence):
    for p_box, p_score, pres in zip(p_boxes, p_scores_img, presence_img):

      if pres == 1:
        detection_image_ids.append(image_id)
        confidences.append(1.)
        valid_bboxes.append([int(p_box[0]), int(p_box[1]), int(p_box[0] + p_box[2]), int(p_box[1] + p_box[3])])

  return np.array(valid_bboxes), np.array(confidences), detection_image_ids


def get_detections_balls_net_card_matlab(detpath):
  detections = pickle.load(open(detpath, 'rb'))

  bboxes = detections['bboxes']
  presence = detections['presence']
  p_scores = detections['p_scores']

  valid_bboxes = []

  for p_boxes, p_scores_img, presence_img in zip(bboxes, p_scores, presence):
    image_bboxes = []

    for p_box, p_score, pres in zip(p_boxes, p_scores_img, presence_img):
      if pres == 1:
        image_bboxes.append([int(p_box[0]), int(p_box[1]), int(p_box[2]), int(p_box[3]), 1.])

    valid_bboxes.append(image_bboxes)

  return np.array(valid_bboxes)


def get_class_recs(classname, imagenames, recs, use_diff):
  class_recs = {}
  npos = 0
  for imagename in imagenames:
    R = [obj for obj in recs[imagename] if obj['name'] == classname]
    bbox = np.array([x['bbox'] for x in R])
    if use_diff:
      difficult = np.array([False for x in R]).astype(np.bool)
    else:
      difficult = np.array([x['difficult'] for x in R]).astype(np.bool)
    det = [False] * len(R)
    npos = npos + sum(~difficult)
    class_recs[imagename] = {'bbox': bbox,
                             'difficult': difficult,
                             'det': det}

  return class_recs, npos


def load_recs(annotations, cachefile, imagenames):
  if not os.path.isfile(cachefile):
    # load annotations
    recs = {}

    for i, (imagename, annotation) in enumerate(zip(imagenames, annotations)):
      recs[imagename] = parse_annotations(annotation)
      if i % 100 == 0:
        print('Reading annotation for {:d}/{:d}'.format(
          i + 1, len(imagenames)))

    # save
    print('Saving cached annotations to {:s}'.format(cachefile))
    with open(cachefile, 'w') as f:
      pickle.dump(recs, f)
  else:
    # load
    with open(cachefile, 'rb') as f:
      try:
        recs = pickle.load(f)
      except:
        recs = pickle.load(f, encoding='bytes')

  return recs


def save_faster_rcnn_res(detpath, nms_val):
  detections_facter_rcnn = get_detections_facter_rcnn_matlab(detpath)
  sio.savemat('det_faster_rcnn_%s.mat' % nms_val, {'det':detections_facter_rcnn})

def eval_frcnn_different_threshold(det_pattern_path, annopath, cachedir, classname, imagesetfile):
  print 'Evaluating FRCNN with different thresholds:'
  nms_thresholds = ['02', '03', '04', '05', '06', '07']
  # nms_thresholds = ['07']
  for threshold in nms_thresholds:
    detpath = det_pattern_path % threshold

    rec, prec, ap = voc_eval(detpath,
                             annopath,
                             imagesetfile,
                             classname,
                             cachedir,
                             get_detects=get_detections_facter_rcnn,
                             ovthresh=0.5,
                             use_07_metric=False,
                             use_diff=False)

    f1 = compute_f1(prec, rec)

    # print "Threshold: {}, rec: {}, prec: {}, ap: {}, f1: {}".format(threshold, rec, prec, ap, f1)
    print "Threshold: {}, ap: {}, f1: {}".format(threshold, ap, f1)


def compute_f1(prec, rec):
  rec_np = np.array(rec)
  prec_np = np.array(prec)
  f1s = 2 * ((prec_np * rec_np) / (prec_np + rec_np))
  f1 = np.max(f1s)
  print 'Argmax f1s: {}'.format(np.argmax(f1s))
  return f1


def eval_yolo_different_threshold(det_pattern_path, annopath, cachedir, classname, imagesetfile):
  print 'Evaluating YOLO with different thresholds:'
  nms_thresholds = ['04', '05', '06', '07', '08', '09']
  for threshold in nms_thresholds:
    detpath = det_pattern_path % threshold

    rec, prec, ap = voc_eval(detpath,
                             annopath,
                             imagesetfile,
                             classname,
                             cachedir,
                             get_detects=get_detections_yolo,
                             ovthresh=0.5,
                             use_07_metric=False,
                             use_diff=False)

    rec_np = np.array(rec)
    prec_np = np.array(prec)

    f1s = 2 * ((prec_np * rec_np) / (prec_np + rec_np))
    f1 = np.max(f1s)
    print 'Argmax f1s: {}'.format(np.argmax(f1s))

    # print "Threshold: {}, rec: {}, prec: {}, ap: {}, f1: {}".format(threshold, rec, prec, ap, f1)
    print "Threshold: {}, ap: {}, f1: {}".format(threshold, ap, f1)

def save_yolo_matlab_different_thresholds(det_pattern_path):
  nms_thresholds = ['04', '05', '06', '07', '08', '09']

  for threshold in nms_thresholds:
    detpath = det_pattern_path % threshold
    detections_yolo = get_detections_yolo_matlab(detpath)
    sio.savemat('det_yolo_%s.mat' % threshold, {'det': detections_yolo})


if __name__ == '__main__':
  annopath = 'permutation-exp/ball_experiment/ds-card-high-overlap2'
  # detpath = 'darkflow/ds-card-high-overlap2/test/out'
  # detpath = 'tf-faster-rcnn/output/default/balls/default/res101_faster_rcnn_iter_300000/detections.pkl'
  # detpath = 'tf-faster-rcnn/output/nms-07/balls/default/res101_faster_rcnn_iter_300000/detections-02.pkl'
  detpath_balls = 'permutation-exp/ball_experiment/detections.pkl'

  cachedir  = 'labels_cache'
  imagesetfile = 'balls_dataset'
  classname = 'object'

  # annotations = read_annotations(annopath)
  # annotations_matlab = parse_annotations_matlab(annotations)

  # sio.savemat('gt_bboxes.mat', {'bboxes': annotations_matlab})

  # detections_balls_net_no_card = get_detections_balls_net_no_card_matlab(detpath_balls)
  # sio.savemat('det_balls_no_card.mat', {'det':detections_balls_net_no_card})

  # detections_balls_net_card = get_detections_balls_net_card_matlab(detpath_balls)
  # sio.savemat('det_balls_card.mat', {'det':detections_balls_net_card})


  # detections_facter_rcnn = get_detections_facter_rcnn_matlab('tf-faster-rcnn/output/default/balls/default/res101_faster_rcnn_iter_300000/detections.pkl')
  # sio.savemat('det_faster_rcnn.mat', {'det':detections_facter_rcnn})
  #
  # detections_yolo = get_detections_yolo_matlab('darkflow/ds-card-high-overlap2/test/out-05')
  # sio.savemat('det_yolo_05.mat', {'det': detections_yolo})

  # frcnn_det_pattern_path = 'tf-faster-rcnn/output/nms-07/balls/default/res101_faster_rcnn_iter_300000/detections-%s.pkl'
  # eval_frcnn_different_threshold(frcnn_det_pattern_path, annopath, cachedir, classname, imagesetfile)

  # save_faster_rcnn_res('tf-faster-rcnn/output/nms-07/balls/default/res101_faster_rcnn_iter_300000/detections-07.pkl',
  #                      '07')
  #
  # results = {}
  #
  rec, prec, _ = voc_eval(detpath_balls,
                           annopath,
                           imagesetfile,
                           classname,
                           cachedir,
                           get_detects=get_detections_balls_net_card,
                           eval_results=evaluate_prec_rec_with_card,
                           ovthresh=0.5,
                           use_07_metric=False,
                           use_diff=False)

  f1 = compute_f1([prec], [rec])

  print "Balls with card. Recall: {}, Precision: {}, F1: {}".format(rec, prec, f1)
  #
  # results['balls_card'] = {'prec': prec, 'rec': rec}
  #
  rec, prec, ap = voc_eval(detpath_balls,
                           annopath,
                           imagesetfile,
                           classname,
                           cachedir,
                           get_detects=get_detections_balls_net_no_card,
                           ovthresh=0.5,
                           use_07_metric=False,
                           use_diff=False)

  f1 = compute_f1(prec, rec)

  print "Balls without card. Average precision: {}, F1: {}".format(ap, f1)
  # results['balls_no_card'] = {'prec':prec, 'rec':rec, 'ap':ap}
  #

  # yolo_det_datapath_pattern = 'darkflow/ds-card-high-overlap2/test/out-%s'
  # eval_yolo_different_threshold(yolo_det_datapath_pattern, annopath, cachedir, classname, imagesetfile)
  # save_yolo_matlab_different_thresholds(yolo_det_datapath_pattern)


  # rec, prec, ap = voc_eval('darkflow/ds-card-high-overlap2/test/out',
  #                          annopath,
  #                          imagesetfile,
  #                          classname,
  #                          cachedir,
  #                          get_detects=get_detections_yolo,
  #                          ovthresh=0.5,
  #                          use_07_metric=False,
  #                          use_diff=False)
  #
  # print "YOLO. Recall: {}, Precision: {}, Average precision: {}".format(rec, prec, ap)
  #
  # results['yolo'] = {'prec':prec, 'rec':rec, 'ap': ap}
  #
  # #
  # rec, prec, ap = voc_eval(detpath,
  #                          annopath,
  #                          imagesetfile,
  #                          classname,
  #                          cachedir,
  #                          get_detects=get_detections_facter_rcnn,
  #                          ovthresh=0.5,
  #                          use_07_metric=False,
  #                          use_diff=False)
  #
  # print "rec: {}, prec: {}, ap: {}".format(rec, prec, ap)
  #
  # results['faster_rcnn'] = {'prec':prec, 'rec':rec, 'ap':ap}
  # print "Faster R-CNN. Recall: {}, Precision: {}, Average precision: {}".format(rec, prec, ap)
  #
  # results_filename = 'results.pkl'
  # with open(results_filename, 'w') as f:
  #   pickle.dump(results, f)

  # print 'aye'