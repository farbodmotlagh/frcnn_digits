try:
    import cPickle as pickle
except ImportError:
    pass

try:
    import _pickle as pickle
except ImportError:
    pass

import _init_paths
import numpy as np
import os
from PIL import Image
from skimage import draw
from datasets.factory import get_imdb


def draw_predicted_bboxes(detect):
    colors_to_use = [(255, 0, 0), (255, 0, 0), (255, 0, 0), (255, 0, 0)]
    datapath = './data/050218/test'
    formatter = '{0:0>' + str(4) + '}'

    img_filenames = np.array(sorted([os.path.join(datapath, fn) for fn in os.listdir(datapath)]))

    #for i in range(detect.shape[0]):
    for i in range(100):
        img = np.array(Image.open(img_filenames[i]).convert('RGB')).astype(np.float32)
        o = False
        for bbox in detect[i, 1]:
            if bbox[4] < 0.0:
                continue
            o = True
            x1 = bbox[0]
            y1 = bbox[1]
            x2 = bbox[2]
            y2 = bbox[3]

            x_coordinates = np.array([x1, x2, x2, x1])
            x_coordinates[x_coordinates > (img.shape[1] - 1)] = img.shape[1] - 1
            x_coordinates[x_coordinates < 0] = 0

            y_coordinates = np.array([y1, y1, y2, y2])
            y_coordinates[y_coordinates > (img.shape[0] - 1)] = img.shape[0] - 1
            y_coordinates[y_coordinates < 0] = 0

            rr, cc = draw.polygon_perimeter(y_coordinates, x_coordinates, shape=img.shape, clip=True)
            draw.set_color(img, (rr, cc), colors_to_use[0])

        if o:
          Image.fromarray(img.astype('uint8')).save(os.path.join('out', 'img' + formatter.format(i) + '.jpg'))

def get_intersection(bbox1, bbox2):
  left = max(bbox1[0] - bbox1[2] / 2., bbox2[0] - bbox2[2] / 2.)
  right = min(bbox1[0] + bbox1[2] / 2., bbox2[0] + bbox2[2] / 2.)
  width = max(right - left, 0)

  top = max(bbox1[1] - bbox1[3] / 2., bbox2[1] - bbox2[3] / 2.)
  bottom = min(bbox1[1] + bbox1[3] / 2., bbox2[1] + bbox2[3] / 2.)
  height = max(bottom - top, 0)

  return width * height

def compute_iou(box1, box2):
    lo_x = max(box1[0], box2[0])
    lo_y = max(box1[1], box2[1])
    hi_x = min(box1[0] + box1[2], box2[0] + box2[2])
    hi_y = min(box1[1] + box1[3], box2[1] + box2[3])

    w = hi_x - lo_x + 1
    h = hi_y - lo_y + 1

    if w < 0 or h < 0:
        intersection = 0
    else:
        intersection = w * h

    union = (box1[2] * box1[3] + box2[2] * box2[3]) - intersection
    iou = intersection / float(union)
    if iou < 0.1:
        print 'Bbox1: {}, Bbox2: {}'.format(box1, box2)
    return iou


def compute_max_overlap(labels):
    ious = np.zeros(len(labels))

    for idx, bboxes in enumerate(labels):
        present_boxes = bboxes[~np.all(bboxes == 0, axis=1)]
        box_count = len(present_boxes)

        if box_count <= 1:
            ious[idx] = 0.
        else:
            image_ious = []
            for i in range(box_count - 1):
                for j in range(i + 1, box_count):
                    image_ious.append(compute_iou(present_boxes[i], present_boxes[j]))

            ious[idx] = np.max(image_ious)

    return ious

def get_bbox_points(current_bbox):
    x_range = np.arange(current_bbox[0], current_bbox[0] + current_bbox[2] + 1)
    y_range = np.arange(current_bbox[1], current_bbox[1] + current_bbox[3] + 1)
    return set([(row, col) for col in x_range for row in y_range])

def compute_max_overlap_multiple_bbox(labels):
    ious = np.zeros(len(labels))

    for idx, bboxes in enumerate(labels):
        present_boxes = bboxes[~np.all(bboxes == 0, axis=1)]
        box_count = len(present_boxes)

        if box_count <= 1:
            ious[idx] = 0.
        else:
            ratios = []

            for i in range(box_count - 1):
                current_bbox = present_boxes[i]
                current_bbox_points = get_bbox_points(current_bbox)

                all_intersections = set()

                for j in range(i + 1, box_count):
                    if i != j:
                        overlapping_bbox_points = get_bbox_points(present_boxes[j])
                        all_intersections.update(current_bbox_points.intersection(overlapping_bbox_points))

                ratios.append(len(all_intersections) / float(len(current_bbox_points)))

            ious[idx] = np.max(ratios)

    return ious

if __name__ == '__main__':
    # draw_predicted_bboxes()

    labels = np.load('./data/050218/test/labels.npz')['a']
    #ious = compute_max_overlap(labels)
    #indices_with_largest_iou = ious.argsort()[::-1]

    detect = np.array(
        pickle.load(open('./output2/res101/digits/default/res101_faster_rcnn_iter_15000/detections.pkl', 'rb'))).T
    imdb = get_imdb('digits_test')
    mse, cardinality_accuracy, wrong_indices, acc = imdb.evaluate_detections(detect, None)
    print acc
    draw_predicted_bboxes(detect)

    #all_indices_set = set(np.arange(0, 5000))
    #wrong_indices_set = set(wrong_indices)

    #right_indices = sorted(list(all_indices_set.difference(wrong_indices_set)))

    #max_iou_right_predicted = np.argmax(ious[right_indices])

    #max_iou_right_predicted_index = right_indices[max_iou_right_predicted]

    #print 'Max right predicted iou index: {}'.format(max_iou_right_predicted_index)

    #misclassified_cardinality_samples = len(wrong_indices)
    #largest_iou_samples_indices = indices_with_largest_iou[:misclassified_cardinality_samples]

    ## print min(ious)

    #intersect = np.intersect1d(wrong_indices, largest_iou_samples_indices)

    #other_ids = sorted(list(set(wrong_indices).difference(set(intersect))))

    ## print other_ids
    #print len(intersect)
